"""
/opt/oracle/Middleware/12c/oracle_common/common/bin/setWlstEnv.sh
/opt/oracle/Middleware/12c/oracle_common/common/bin/wlst.sh
connect('weblogic','weblogic1','t3://localhost:7001')
execfile('/vagrant/createuser.py')
disconnect()
exit()
"""
import string
import random

def generate_password(size=12, chars=string.ascii_letters + string.digits):
	# Weblogic constrain: user password must contain atleast 1 letter and atleast 1 digit
    password = ''
    password += random.choice(string.ascii_letters)
    password += random.choice(string.digits)
    for i in range(size-2):
    	password += random.choice(chars)
    return password


users = ['username@domain.com','anotherone@domain.com', 'yet.another.one@domain.com']
group = 'MyTeam'
groupprefixes = ['PREF_', 'PRE_', 'PR_', 'P_']


serverConfig()
print 'lookup DefaultAuthenticator' 
atnr=cmo.getSecurityConfiguration().getDefaultRealm().lookupAuthenticationProvider('DefaultAuthenticator')


print 'Users:'
for user in users:
	user = string.split(user, sep="@")[0].lower()  # Strip the domain part after the @ if it's an emailadres
	if not atnr.userExists(user):
		password = generate_password()
		atnr.createUser(user, password, 'Team ' + group)
		print user + ', ' + password
	else:
		print user + ', (user already exists: no new password)'

print 'Groups:'
for prefix in groupprefixes:
	groupname = prefix + group
	if not atnr.groupExists( groupname ): atnr.createGroup( groupname, groupname )
	for user in users:
		user = string.split(user, sep="@")[0].lower() # Strip the domain part after the @ if it's an emailadres
		if not atnr.isMember(groupname,user,true): atnr.addMemberToGroup( groupname, user )
	print groupname